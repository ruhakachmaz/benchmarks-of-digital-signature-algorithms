from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, ec, dsa, padding
import time
import sys

rsa_key_lens = [2048, 3072, 4096, 7680]
dsa_key_lens = [2048, 3072, 4096]
ecdsa_curves = [ec.SECP256R1, ec.SECP256K1, ec.SECP384R1]

rsa_key_len = rsa_key_lens[int(sys.argv[1])]
dsa_key_len = dsa_key_lens[int(sys.argv[2])]
ecdsa_curve = ecdsa_curves[int(sys.argv[3])]

dsa_hash_alg = hashes.SHA384 if dsa_key_len == 4096 else hashes.SHA256
ecdsa_hash_alg = hashes.SHA384 if ecdsa_curve == ec.SECP384R1  else hashes.SHA256

# Function to sign a message using RSA
def sign_with_rsa(private_key, message):
    return private_key.sign(
        message,
        padding.PKCS1v15(),
        hashes.SHA256()
    )

# Function to sign a message using ECDSA
def sign_with_ecdsa(private_key, message):
	return private_key.sign(
		message,
		ec.ECDSA(ecdsa_hash_alg())
	)

# Function to sign a message using DSA
def sign_with_dsa(private_key, message):
	return private_key.sign(
		message,
		dsa_hash_alg()
	)


# Generate one key pair for each algorithm
rsa_private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=rsa_key_len,
    backend=default_backend()
)

ecdsa_private_key = ec.generate_private_key(
    curve=ecdsa_curve(),
    backend=default_backend()
)

dsa_private_key = dsa.generate_private_key(
    key_size=dsa_key_len,
    backend=default_backend()
)

# Number of iterations
iterations = int(sys.argv[4])
message = b"Message to sign"

# Time RSA signing
start_time = time.time()
for _ in range(iterations):
    sign_with_rsa(rsa_private_key, message)
rsa_time = time.time() - start_time
rsa_avg_time = rsa_time / iterations

# Time ECDSA signing
start_time = time.time()
for _ in range(iterations):
    sign_with_ecdsa(ecdsa_private_key, message)
ecdsa_time = time.time() - start_time
ecdsa_avg_time = ecdsa_time / iterations

# Time DSA signing
start_time = time.time()
for _ in range(iterations):
    sign_with_dsa(dsa_private_key, message)
dsa_time = time.time() - start_time
dsa_avg_time = dsa_time / iterations

# Display the average times
print(f"Average RSA signing time: {rsa_avg_time:.6f} seconds, signing per second: {1 / rsa_avg_time}")
print(f"Average DSA signing time: {dsa_avg_time:.6f} seconds, signing per second: {1 / dsa_avg_time}")
print(f"Average ECDSA signing time: {ecdsa_avg_time:.6f} seconds, signing per second: {1 / ecdsa_avg_time}")
