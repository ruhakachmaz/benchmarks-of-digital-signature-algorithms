from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, ec, dsa, padding
import time
import sys

rsa_key_lens = [2048, 3072, 4096, 7680]
dsa_key_lens = [2048, 3072, 4096]
ecdsa_curves = [ec.SECP256R1, ec.SECP256K1, ec.SECP384R1]

rsa_key_len = rsa_key_lens[int(sys.argv[1])]
dsa_key_len = dsa_key_lens[int(sys.argv[2])]
ecdsa_curve = ecdsa_curves[int(sys.argv[3])]

# Generate a key pair for RSA, ECDSA, and DSA
rsa_private_key = rsa.generate_private_key(
    public_exponent=65537, key_size=rsa_key_len, backend=default_backend()
)

ecdsa_private_key = ec.generate_private_key(
    curve=ecdsa_curve(), backend=default_backend()
)

dsa_private_key = dsa.generate_private_key(
    key_size=dsa_key_len, backend=default_backend()
)

dsa_hash_alg = hashes.SHA384 if dsa_key_len == 4096 else hashes.SHA256
ecdsa_hash_alg = hashes.SHA384 if ecdsa_curve == ec.SECP384R1  else hashes.SHA256

message = b"0"*128

rsa_signature = rsa_private_key.sign(
    message,
	padding.PKCS1v15(),
    hashes.SHA256()
)

ecdsa_signature = ecdsa_private_key.sign(
    message, 
    ec.ECDSA(ecdsa_hash_alg())
)

dsa_signature = dsa_private_key.sign(
    message, 
    dsa_hash_alg()
)

# Function to verify RSA signature
def verify_rsa(public_key, message, signature):
    public_key.verify(
        signature,
        message,
		padding.PKCS1v15(),
        hashes.SHA256()
    )

# Function to verify ECDSA signature
def verify_ecdsa(public_key, message, signature):
    public_key.verify(
        signature,
        message,
        ec.ECDSA(ecdsa_hash_alg())
    )

# Function to verify DSA signature
def verify_dsa(public_key, message, signature):
    public_key.verify(
        signature,
        message,
        dsa_hash_alg()
    )

iterations = int(sys.argv[4])

# Time RSA signature verification
start_time = time.time()
for _ in range(iterations):
    verify_rsa(rsa_private_key.public_key(), message, rsa_signature)
rsa_verify_time = time.time() - start_time
rsa_avg_verify_time = rsa_verify_time / iterations

# Time ECDSA signature verification
start_time = time.time()
for _ in range(iterations):
    verify_ecdsa(ecdsa_private_key.public_key(), message, ecdsa_signature)
ecdsa_verify_time = time.time() - start_time
ecdsa_avg_verify_time = ecdsa_verify_time / iterations

# Time DSA signature verification
start_time = time.time()
for _ in range(iterations):
    verify_dsa(dsa_private_key.public_key(), message, dsa_signature)
dsa_verify_time = time.time() - start_time
dsa_avg_verify_time = dsa_verify_time / iterations

print(f"Average RSA signature verification time: {rsa_avg_verify_time:.6f} seconds, verify per second: {1 / rsa_avg_verify_time}")
print(f"Average DSA signature verification time: {dsa_avg_verify_time:.6f} seconds, verify per second: {1 / dsa_avg_verify_time}")
print(f"Average ECDSA signature verification time: {ecdsa_avg_verify_time:.6f} seconds, verify per second: {1 / ecdsa_avg_verify_time}")