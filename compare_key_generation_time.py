from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, ec, dsa
import time
import sys

rsa_key_lens = [2048, 3072, 4096, 7680]
dsa_key_lens = [2048, 3072, 4096]
ecdsa_curves = [ec.SECP256R1, ec.SECP256K1, ec.SECP384R1]

rsa_key_len = rsa_key_lens[int(sys.argv[1])]
dsa_key_len = dsa_key_lens[int(sys.argv[2])]
ecdsa_curve = ecdsa_curves[int(sys.argv[3])]

# Function to generate RSA keys
def generate_rsa_key(key_size=rsa_key_len):
    return rsa.generate_private_key(
        public_exponent=65537,
        key_size=key_size,
        backend=default_backend()
    )

# Function to generate ECDSA keys
def generate_ecdsa_key(curve=ecdsa_curve()):
    return ec.generate_private_key(
        curve=curve,
        backend=default_backend()
    )

# Function to generate DSA keys
def generate_dsa_key(key_size=dsa_key_len):
    return dsa.generate_private_key(
        key_size=key_size,
        backend=default_backend()
    )

# Number of iterations
iterations = int(sys.argv[4])

# Time RSA key generation
start_time = time.time()
for _ in range(iterations):
    generate_rsa_key(rsa_key_len)
rsa_time = time.time() - start_time
rsa_avg_time = rsa_time / iterations

# Time ECDSA key generation
start_time = time.time()
for _ in range(iterations):
    generate_ecdsa_key(ecdsa_curve())
ecdsa_time = time.time() - start_time
ecdsa_avg_time = ecdsa_time / iterations

# Time DSA key generation
start_time = time.time()
for _ in range(iterations):
    generate_dsa_key(dsa_key_len)
dsa_time = time.time() - start_time
dsa_avg_time = dsa_time / iterations

# Display the average times
print(f"Average RSA key generation time: {rsa_avg_time:.6f} seconds, keys per second: {1 / rsa_avg_time}")
print(f"Average DSA key generation time: {dsa_avg_time:.6f} seconds, keys per second: {1 / dsa_avg_time}")
print(f"Average ECDSA key generation time: {ecdsa_avg_time:.6f} seconds, keys per second: {1 / ecdsa_avg_time}")
