# Benchmarks of digital signature algorithms RSA, DSA and ECDSA



## Scripts

1) compare_key_generation_time.py 
2) compare_signing_time.py
3) compare_verification_time.py

## Usage

We have params:

rsa_key_lens = [2048, 3072, 4096, 7680]  
dsa_key_lens = [2048, 3072, 4096]  
ecdsa_curves = [ec.SECP256R1, ec.SECP256K1, ec.SECP384R1]  

So we can use them all. For example, compare signing time for RSA 2048, ECDSA SECP256K1 and DSA 4096 for 100 times:

compare_signing_time.py 0 1 2 100



